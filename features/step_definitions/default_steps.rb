# encoding: utf-8

Dado(/^que eu esteja no site do Walmart$/) do
	@browser.goto "walmart.com.br"
end

Dado(/^clico no menu "(.*?)"$/) do |menu|
	@browser.link(:text, menu).when_present.click
end

Dado(/^eu clico no link "(.*?)"$/) do |link|
	@browser.link(:text, link).when_present.click
end

Dado(/^eu preencho o CPF "(.*?)"$/) do |cpf|
	@browser.text_field(:id, 'ctl00_Conteudo_txtCpfCnpj').when_present.set cpf
end

Dado(/^eu preencho o CEP "(.*?)"$/) do |cep|
  	@browser.text_field(:id, 'ctl00_Conteudo_txtCep').when_present.set cep[0..4]
  	@browser.text_field(:id, 'ctl00_Conteudo_txtCepSufixo').when_present.set cep[6..8]
end

Dado(/eu clico no botao Continuar/) do
	@browser.link(:id, 'ctl00_Conteudo_btnBuscaEmail').when_present.click
end

Entao(/^eu devo ver a mensagem "(.*?)"$/) do |msg|
	@browser.div(:class, 'email-sucesso').when_present.text.include? msg
end
