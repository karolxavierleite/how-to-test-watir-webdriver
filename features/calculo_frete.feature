Feature: Calculo de Frete
	Como usuario de e-commerce
	Para estimar o valor das minhas compras
	Eu quero poder calcular o frete em diversos pontos do site, antes de avancar com o pedido

	Scenario: Calcular frete no carrinho
		Given que eu esteja no site do Walmart
		And eu adiciono um produto qualquer no carrinho
		And eu preencho o campo de CEP com "01415-000"
		And clico no botao "Calcular"
		Then eu devo ver o "Valor do Frete" com o valor "Frete Gratis"
    